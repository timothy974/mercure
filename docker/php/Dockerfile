# Dockerfile located in docker/php/Dockerfile
FROM php:8.2-fpm

# Install system dependencies
RUN apt-get update && apt-get install -y \
        libpq-dev \
        libzip-dev \
        zip \
        git \
        unzip \
        curl

# Install PHP extensions for PostgreSQL
RUN docker-php-ext-install \
        pdo \
        pdo_pgsql \
        zip

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Set working directory
WORKDIR /var/www

# Copy the application code to the container (assumes your application is in the same directory as the Dockerfile)
COPY . /var/www

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]
