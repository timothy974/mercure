# If the first argument is "run"...
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),build up start))
  # use the rest as arguments for "run"
  RUN_ARGS := $(subst :,\:,$(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS)))
  RUN_ARGS := $(subst -,\-,$(RUN_ARGS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

build:
	docker-compose $(DOCKERFILE) build $(RUN_ARGS)

up :
	docker-compose $(DOCKERFILE) up -d --force-recreate --remove-orphans $(RUN_ARGS)

down :
	docker-compose $(DOCKERFILE) down

start :
	docker-compose $(DOCKERFILE) start $(RUN_ARGS)

stop :
	docker-compose $(DOCKERFILE) stop

c-c:
	docker-compose exec -e XDEBUG_MODE=off php bin/console cache:pool:clear cache.global_clearer
	docker-compose exec -e XDEBUG_MODE=off php bin/console c:c

trans:
	docker-compose exec -e XDEBUG_MODE=off php bin/console app:translation:extract

reset:
	docker-compose exec -e XDEBUG_MODE=off php php bin/console d:d:d --force --if-exists
	docker-compose exec -e XDEBUG_MODE=off php php bin/console d:d:c
	docker-compose exec -e XDEBUG_MODE=off php php bin/console d:m:m --no-interaction