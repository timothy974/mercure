<?php

namespace App\Command;

use App\Entity\Import;
use App\Repository\ImportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:create:import'
)]
class CreateImportCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ImportRepository $importRepository
    ) {
        parent::__construct('ImportCommand');
    }

    protected function configure(): void
    {
        $this
            ->setDescription(
                'Crée un import.'
            )
            ->setHelp('Cette commande permet de créer un import');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        for ($i = 0; $i < 5; $i++) {
            if (empty($this->importRepository->findBy(['name' => 'test_import_' . $i]))) {
                $import = new Import();
                $import
                    ->setPercentProgress($i)
                    ->setName('test_import_' . $i)
                    ->addTopic('test_import_' .$i)
                ;

                $this->entityManager->persist($import);
                $this->entityManager->flush();

                $io->success("L'import a bien été créé");
            }
        }

        return Command::SUCCESS;
    }
}
