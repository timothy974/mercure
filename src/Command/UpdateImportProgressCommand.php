<?php

namespace App\Command;

use App\Entity\Import;
use App\Repository\ImportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Random\RandomException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\HubRegistry;
use Symfony\Component\Mercure\Update;

#[AsCommand(
    name: 'app:update:import'
)]
class UpdateImportProgressCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ImportRepository $importRepository,
        private readonly HubInterface $hub
    ) {
        parent::__construct('UpdateImportProgress');
    }

    protected function configure(): void
    {
        $this
            ->setDescription(
                'Fait progresser les imports'
            )
            ->setHelp('Cette commande permet de faire progresser les imports');
    }

    /**
     * @throws \JsonException
     * @throws RandomException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $imports = $this->importRepository->findAll();

        foreach ($imports as $import) {
            while ($import->getPercentProgress() < 100) {
                usleep(100);
                $percent = $import->getPercentProgress() + random_int(-1, 2);
                if ($percent < 0) {
                    $percent = 0;
                }

                if ($percent > 100) {
                    $percent = 100;
                }

                $import->setPercentProgress($percent);

                $this->entityManager->flush();

                $update = new Update(
                    $import->getTopics()[0],
                    json_encode([
                        'importId' => $import->getId(),
                        'importProgress' => $import->getPercentProgress()
                    ], JSON_THROW_ON_ERROR)
                );

                $this->hub->publish($update);
            }
        }

        return Command::SUCCESS;
    }
}
