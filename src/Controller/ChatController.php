<?php

namespace App\Controller;

use App\Entity\Chat;
use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class ChatController extends AbstractController
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    #[Route('/create-chat/{id}', name: 'app_create_chat')]
    public function createChat(User $receiver): RedirectResponse
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        // Ajout des 2 utilisateurs faisant parti du chat
        $chat = new Chat();
        $chat
            ->addUser($currentUser)
            ->addUser($receiver);

        // Persist permettant de générer l'id du chat
        $this->entityManager->persist($chat);

        $chat->addTopic($this->getParameter('mercure_prefix_topic') . '_chat_' . $chat->getId());

        $this->entityManager->flush();

        return $this->redirectToRoute('app_chat', [
            'id' => $chat->getId()
        ]);
    }

    #[Route('/chat/{id}', name: 'app_chat')]
    public function chatBox(
        Request $request,
        HubInterface $hub,
        Chat $chat
    ): Response {
        /** @var User $currentUser */
        $currentUser = $this->getUser();
        $otherUser = $chat->findOtherUser($currentUser);

        if ($otherUser === null) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(MessageType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $message = new Message();
            $message
                ->setSender($currentUser)
                ->setReceiver($otherUser)
                ->setChat($chat)
                ->setMessage($form->get('message')->getData());

            $this->entityManager->persist($message);
            $this->entityManager->flush();

            $update = new Update(
                $chat->getTopics()[0],
                json_encode([
                    'message' => $message->getMessage(),
                    'sender' => $message->getSender()->getEmail(),
                    'senderId' => $message->getSender()->getId()
                ], JSON_THROW_ON_ERROR),
                true
            );

            $hub->publish($update);

            return $this->redirectToRoute('app_chat', [
                'id' => $chat->getId()
            ]);
        }

        $users = $this->entityManager->getRepository(User::class)->findAll();

        return $this->render('chat/index.html.twig', [
            'user' => $currentUser,
            'chat' => $chat,
            'form' => $form->createView(),
            'users' => $users,
        ]);
    }
}
