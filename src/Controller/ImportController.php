<?php

namespace App\Controller;

use App\Entity\Chat;
use App\Entity\Import;
use App\Entity\Message;
use App\Entity\User;
use App\Form\MessageType;
use App\Repository\ImportRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mercure\HubInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Routing\Annotation\Route;

class ImportController extends AbstractController
{
    #[Route('/imports', name: 'app_import')]
    public function imports(
        ImportRepository $importRepository
    ): Response {

        $imports = $importRepository->findAll();

        return $this->render('import/index.html.twig', [
            'imports' => $imports
        ]);
    }
}
