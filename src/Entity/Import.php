<?php

namespace App\Entity;

use App\Repository\ImportRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: ImportRepository::class)]
class Import
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::STRING)]
    private string $name;

    #[ORM\Column(type: Types::INTEGER)]
    #[Assert\Range(min: 0, max: 100)]
    private int $percentProgress = 0;

    #[ORM\Column(type: Types::SIMPLE_ARRAY)]
    private array $topics = [];

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPercentProgress(): int
    {
        return $this->percentProgress;
    }

    public function setPercentProgress(int $percentProgress): self
    {
        $this->percentProgress = $percentProgress;

        return $this;
    }

    public function getTopics(): array
    {
        return $this->topics;
    }

    public function setTopics(array $topics): self
    {
        $this->topics = $topics;

        return $this;
    }

    public function addTopic(string $topic): static
    {
        if (!in_array($topic, $this->topics, true)) {
            $this->topics[] = $topic;
        }

        return $this;
    }
}
