<?php

namespace App\Mercure;

use App\Entity\User;
use App\Repository\ImportRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Mercure\Jwt\LcobucciFactory;
use Symfony\Component\Mercure\Jwt\TokenFactoryInterface;

final class TokenFactory implements TokenFactoryInterface
{
    private LcobucciFactory $lcobucciFactory;

    public function __construct(
        private readonly Security $security,
        string $jwtSecret,
        private readonly ImportRepository $importRepository
    ) {
        $this->lcobucciFactory = new LcobucciFactory($jwtSecret);
    }

    public function create(?array $subscribe = [], ?array $publish = [], array $additionalClaims = []): string
    {
        /** @var User $user */
        $user = $this->security->getUser();

        $imports = $this->importRepository->findAll();

        $subscribe = $publish = [];

        if ($user instanceof User) {
            foreach ($user->getChats() as $chat) {
                $subscribe[] = $chat->getTopics()[0];
                $publish[] = $chat->getTopics()[0];
            }

            foreach ($imports as $import) {
                $subscribe[] = $import->getTopics()[0];
            }
        }

        if (PHP_SAPI === 'cli') {
            foreach ($imports as $import) {
                $publish[] = $import->getTopics()[0];
            }
        }


        return $this->lcobucciFactory->create($subscribe, $publish, $additionalClaims);
    }
}
